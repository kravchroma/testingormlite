package com.example.rom_pc.tormlite.dao;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by ROM_PC on 13.02.2016.
 */
public class ProductDAO extends BaseDaoImpl<Product, Integer> {

    protected ProductDAO(ConnectionSource connectionSource,
                         Class<Product> dataClass) throws SQLException {
        super(connectionSource, dataClass);
    }

    public List<Product> getAllProduct() throws SQLException{

        return this.queryForAll();
    }

    public void overvaluationProduct(int procent) throws SQLException {

        for(Product item : this.getAllProduct()) {
            final double newPrice = item.getPrice() + item.getPrice() * procent / 100;
            item.setPrice(newPrice);
            this.update(item);
        }
    }
}
