package com.example.rom_pc.tormlite;

import android.app.Application;
import android.content.Context;

import com.example.rom_pc.tormlite.dao.HelperSingleton;

/**
 * Created by ROM_PC on 13.02.2016.
 */
public class MyApp  extends Application{

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    @Override
    public void onTerminate() {
        HelperSingleton.getInstance().releaseHelper();
        super.onTerminate();
    }

    public static Context getContext() {
        return context;
    }
}
