package com.example.rom_pc.tormlite.dao;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;


/**
 * Created by ROM_PC on 13.02.2016.
 */
@DatabaseTable(tableName = "goals")
public class Product {

    public final static String GOAL_NAME_FIELD_NAME = "name";

    @DatabaseField(generatedId = true)
    private int Id;

    @DatabaseField(canBeNull = false, dataType = DataType.STRING, columnName = GOAL_NAME_FIELD_NAME)
    private String name;

    @DatabaseField(dataType = DataType.DATE)
    private Date lastEditDate;

    @DatabaseField()
    private String notes;

    @DatabaseField
    private double price;

    public Product(String name, Date lastEditDate, String notes, double price) {
        this.name = name;
        this.lastEditDate = lastEditDate;
        this.notes = notes;
        this.price = price;
    }

    public Product() {
    }

    public int getId() {
        return Id;
    }

    public String getName() {
        return name;
    }

    public Date getLastEditDate() {
        return lastEditDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }
}

