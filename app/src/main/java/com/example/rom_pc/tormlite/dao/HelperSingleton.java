package com.example.rom_pc.tormlite.dao;

import android.content.Context;

import com.example.rom_pc.tormlite.MyApp;
import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * Created by ROM_PC on 13.02.2016.
 */
public class HelperSingleton {

    private static HelperSingleton instance = new HelperSingleton();

    private DatabaseHelper databaseHelper;

    private HelperSingleton() {
        setHelper(MyApp.getContext());
    }

    public static HelperSingleton getInstance() {
        return instance;
    }

    public DatabaseHelper getHelper(){
        return databaseHelper;
    }
    private void setHelper(Context context){
        databaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
    }
    public void releaseHelper(){
        OpenHelperManager.releaseHelper();
        databaseHelper = null;
    }
}
