package test;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.View;

import com.example.rom_pc.tormlite.BuildConfig;
import com.example.rom_pc.tormlite.dao.Product;
import com.example.rom_pc.tormlite.dao.HelperSingleton;
import com.example.rom_pc.tormlite.dao.ProductDAO;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.matches;
import static org.mockito.Mockito.when;

/**
 * Created by ROM_PC on 13.02.2016.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class,
        manifest = "AndroidManifest.xml",
        sdk = 16)
public class appTest {

    @Test
    public void testInit() throws Exception {

        HelperSingleton.getInstance()
                .getHelper()
                .getProductDAO()
                .create(new Product("Ноутбук HP 255", new Date(), "Ноутбук", 1200.52));
        HelperSingleton.getInstance()
                .getHelper()
                .getProductDAO()
                .create(new Product("iPhone 5C", new Date(), "Телефон", 800.60));
        HelperSingleton.getInstance()
                .getHelper()
                .getProductDAO()
                .create(new Product("Lenovo Tab 2", new Date(), "Планшеты", 930.40));

        List<Product> products = HelperSingleton.getInstance()
                .getHelper()
                .getProductDAO()
                .getAllProduct();

        assertNotNull(products);
        assertTrue(!products.isEmpty());
        assertTrue(products.size() == 3);

        testOverValProduct();
    }

    public void testOverValProduct() throws Exception {

        List<Double> list = new ArrayList<>();
        final int procent = 20;

        ProductDAO productDAO = HelperSingleton.getInstance()
                                        .getHelper()
                                        .getProductDAO();

        //TMP
        for(Product product : productDAO.getAllProduct()) {
            list.add(product.getPrice());
        }

        productDAO.overvaluationProduct(procent);

        List<Product> products = productDAO.getAllProduct();
        for(int i = 0; i < products.size() ; i++) {
            double pr = (products.get(i).getPrice() - list.get(i))/list.get(i)*100;
            assertEquals(procent, (int)pr);
        }
    }

    @Mock
    Context fakeContext;

    @Mock
    Resources resources;

    @Test
    public void  testMockito() {

        MockitoAnnotations.initMocks(this);
        DisplayMetrics metrics = new DisplayMetrics();
        metrics.density = 500;
        when(fakeContext.getResources()).thenReturn(resources);
        when(fakeContext.getResources().getDisplayMetrics()).thenReturn(metrics);
        View view = new View(fakeContext);

    }
}
